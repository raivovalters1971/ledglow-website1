
<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

ob_start();

session_start();

/* DB Configrations */
define('HOST','localhost');
define('USER','root');
define('PASSWORD','');
define('DB_NAME','car_shop');

$connection =  @mysqli_connect(HOST,USER,PASSWORD,DB_NAME);

// Checking connection
if(mysqli_connect_errno())
{
	echo "<center><p style='color:red; margin-top:10%;'>Failed to connect to MySQL Database : " . mysqli_connect_error($connection)." ..... !!</p></center>";
	exit;
}

define( 'PROJECT_TITLE', 'LEDGLOW' );
define( 'PROJECT_NAME', 'LEDGLOW' );
define( 'COPYRIGHT_YEAR', '2020' );

if(!isset($_SESSION['cart']))
{
	$_SESSION['cart'] = array();
}

?>