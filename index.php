<?php
include_once( dirname(__FILE__). '/includes/config.php' );
include_once( dirname(__FILE__). '/includes/code_till_body_tag.php' );
include_once( dirname(__FILE__). '/includes/nav.php' );
?>

<style>
#li_1
{
  background: #FF33547;
  transition: .10s;
}
@media (max-width: 800px)
{
  #li_1
  {
    background: none;
    color: #9DFF33;
  }
}
</style>

<section class="section_1"></section>
<br /><br />
<div style="padding: 5px;">
  <center>
    <h1>Sveicināts <?php echo PROJECT_NAME; ?></h1>
    <br />
    <p style="text-align: center;">Piedāvājam plašu klāstu ar auto led apgaismojumu tieši no ražotāja. Lētākās cenas un augstākā kvalitāte garantēta</p>
    
  </center>
</div>
<br /><br /><br />

<?php
include_once( dirname(__FILE__). '/includes/footer.php' );
?>