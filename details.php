<?php
include_once( dirname(__FILE__). '/includes/config.php' );
include_once( dirname(__FILE__). '/includes/code_till_body_tag.php' );
include_once( dirname(__FILE__). '/includes/nav.php' );
?>

<div class="about-section">
  <h1>Lūdzu norādīt personas datus un adresi uz kuru piegādāt sūtījumu, kā arī veiciet samaksu par pirkumu</h1>
</div>

<div class="about-section-2">
  <form action="details_submit.php" method="GET" onsubmit="form_submit();">
    <b>Norādiet personas datus un piegādes adresi:</b>
    <br /><br />
    <label>Vārds</label>
    <input type="text" required />
    <label>Uzvārds</label>


    <input type="text" required />

    <label>Izvēlieties piegādes veidu</label>
    <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="credit">Omniva pakomāts</label>
              </div>
              <div class="custom-control custom-radio">
                <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="debit">Latvijas Pasts</label>
              </div>
              <div class="custom-control custom-radio">
                <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="paypal">DPD paku bode</label>
              </div>

              <label>Piegādes adrese</label>
    <input type="text" required />


    <label>Epasta adrese</label>
    <input type="email" required />
    <label>Telefona numurs</label>
    <input type="text" required />
    <br /><br />
   



    <center>
    <img style="width: 500px;" src="img/det.png" />
    </center>
    <b>Lūdzu ievadiet kredītkartes datus:</b>
   
    <br /><br />
  
    <label>Kartes turētāja vārds un uzvārds</label>
    <input type="text" required />
    <label>Kartes numurs</label>
    <input type="text" required />
    <label>Derīguma termiņš (mm/yy)</label>
    <input type="text" required />
    <label>CC/Sec Code</label>
    <input type="text" required />
    <br /><br />
  
    <input  type="submit" name="field" value="Nosūtī!" class="button button2" style="padding: 10px;">
  </form>
</div>

<br /><br /><br /><br />

<script>
function form_submit()
{
  window.location.href = "details_submit.php";
  return false;
}
</script>

<?php
include_once( dirname(__FILE__). '/includes/footer.php' );
?>