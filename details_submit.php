<?php
include_once( dirname(__FILE__). '/includes/config.php' );
include_once( dirname(__FILE__). '/includes/code_till_body_tag.php' );
include_once( dirname(__FILE__). '/includes/nav.php' );
?>

<div style="padding: 20px; margin-top: 75px; margin-bottom: 175px;">
  <center>
    <img style="width: 75px;" src="img/tick.png" />
    <br /><br />
    <h1>Sūtījums pieņemts!</h1>
    <br />
    <h1>Jūsu prece drīz tiks nodota Jūsu izvēlētajam kurjeram.</h1>
    <br />
    <h1>Paldies ka iepirkies pie mums!</h1>
  </center>
</div>

<?php
unset ($_SESSION['cart']);
?>

<?php
include_once( dirname(__FILE__). '/includes/footer.php' );
?>