<?php
include_once( dirname(__FILE__). '/includes/config.php' );
include_once( dirname(__FILE__). '/includes/code_till_body_tag.php' );
include_once( dirname(__FILE__). '/includes/nav.php' );
?>

<?php
if(isset($_GET['add_to_cart']))
{
  $auto_id =  (int) $_GET['add_to_cart'];

	$query_1 = "SELECT * FROM products WHERE auto_id='$auto_id'";
	$result_1 = mysqli_query($connection, $query_1);
  $affected_rows_1 = $connection->affected_rows;
    
  if(!$result_1)
  {
      echo "Error: " . mysqli_error($connection)." ..... !!";
      exit;
  }
  else if ($affected_rows_1 == 0)
  {
    ?>
    <script>
    window.alert('Invalid Product Detail.');
    window.location.href = "shop.php";
    </script>
    <?php
  }
  else
  {
    if (in_array($auto_id, $_SESSION['cart']))
    {
      ?>
      <script>
      window.alert('Product Already added to the Cart.');
      window.location.href = "shop.php";
      </script>
      <?php
    }
    else
    {
      array_push($_SESSION['cart'] , $auto_id);
      ?>
      <script>
     /* window.alert('Produkts tika pievienots grozam.');*/
      /*window.location.href = "shop.php";*/
      </script>
      <?php
    }
    
  }
}
?>

<style>
#li_2
{
  background: #1b9bff;
  transition: .5s;
}
@media (max-width: 858px)
{
  #li_2
  {
    background: none;
    color: #0082e6;
  }
}
</style>

<link href="css/style_2.css" media="all" rel="stylesheet" />

<div class="about-section">
  <h1>Veikals</h1>
  <br /><br />

  <div class="col-sm-9 padding-right" style="margin-bottom: 70px;">
    <div class="features_items">
      <?php
      $select_query = "SELECT * FROM products";
      $select_result = mysqli_query($connection,$select_query);
      $affected_rows = $connection->affected_rows;
      if(!$select_result)
      {
        echo "<p>Error: " . mysqli_error($connection)." ..... !!</p>";
      }
      else if($affected_rows == 0)
      {
        echo "<p>No Product Exist ..... !!</p>";
      }
      else
      {
        $serial_no = 1;
        while ( $row = mysqli_fetch_array($select_result) )
        {
          $auto_id = $row['auto_id'];
          $title = $row['title'];
          $image = $row['image'];
          $price = $row['price'];
          ?>
      <div class="col-sm-4">
        <div class="product-image-wrapper">
          <div class="single-products">
            <div class="productinfo text-center">
              <img src="data:image/jpeg;base64,<?php echo base64_encode($image); ?>" alt="<?php echo $title; ?>" />
              <h2>$<?php echo $price; ?></h2>
              <p style="font-weight: bold;"><?php echo $title; ?></p>

              <?php
              if (in_array($auto_id, $_SESSION['cart']))
              {
                ?>
                <a nohref class="btn btn-default add-to-cart" style="background-color: green; color: white;">Produkts jau ir ievietots grozā</a>
                <?php
              }
              else
              {
                ?>
                <a href="shop.php?add_to_cart=<?php echo $auto_id; ?>" class="btn btn-default add-to-cart">Pievienot grozam</a>
                <?php
              }
              ?>

            </div>
          </div>
        </div>
      </div>
          <?php
        }
      }
      ?>
    </div>
  </div>
</div>

<?php
include_once( dirname(__FILE__). '/includes/footer.php' );
?>