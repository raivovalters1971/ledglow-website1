<?php
include_once( dirname(__FILE__). '/includes/config.php' );
include_once( dirname(__FILE__). '/includes/code_till_body_tag.php' );
include_once( dirname(__FILE__). '/includes/nav.php' );
?>

<style>
#li_3
{
  background: #FDF105;
  transition: .5s;
}
@media (max-width: 858px)
{
  #li_3
  {
    background: #FDF105;
    color: #FDF105;
  }
}
</style>

<div class="about-section">
  <h1>Iepirkumu grozs</h1>
  <br />
  <center>
    <table id="table_1">
    <tr>
      <th>Nosaukums</th>
      <th>Attēls</th>
      <th>Cena (EUR)</th>
    </tr>
    <?php
    if(count($_SESSION['cart']) == 0)
    {
      ?>
      <tr>
        <td colspan="3">Jūsu grozs ir tukšs</th>
      </tr>
      <?php
    }
    else
    {
      for($i=0; $i<count($_SESSION['cart']); $i++)
      {
        $auto_id = $_SESSION['cart'][$i];
        $query_1 = "SELECT * FROM products WHERE auto_id='$auto_id'";
        $result_1 = mysqli_query($connection, $query_1);
        $row_1 = mysqli_fetch_array($result_1);
        $title = $row_1['title'];
        $image = $row_1['image'];
        $price = $row_1['price'];
        ?>
        <tr>
          <td><?php echo $title; ?></td>
          <td>
            <img src="data:image/jpeg;base64,<?php echo base64_encode($image); ?>" style="width: 75px;" alt="<?php echo $title; ?>" />
          </td>
          <td><?php echo $price; ?></td>
        </tr>
        <?php
      }
    }
    ?>
  </table>
  <?php
  if(count($_SESSION['cart']) != 0)
  {
    ?>
    <a class="button button2" href="details.php" style="margin-top: 10px; padding: 10px; float: right;">Pasūtīt</a>
    <?php
  }
  ?>
</center>
</div>
<br />

<?php
include_once( dirname(__FILE__). '/includes/footer.php' );
?>